//
//  MB_ValentineDayLayerRank.h
//  ProjectMB
//
//  Created by wenyong on 15-1-22.
//
//

#ifndef __ProjectMB__MB_ValentineDayLayerRank__
#define __ProjectMB__MB_ValentineDayLayerRank__

#include "MB_LayerFestivalRank.h"

class MB_ValentineDayLayerRank : public MB_LayerFestivalRank
{
public:
    virtual bool init();
};

#endif /* defined(__ProjectMB__MB_ValentineDayLayerRank__) */
