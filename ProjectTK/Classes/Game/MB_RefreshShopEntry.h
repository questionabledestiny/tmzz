//
//  MB_RefreshShopEntry.h
//  ProjectMB
//
//  Created by chenhongkun on 14-9-5.
//
//

#ifndef __ProjectMB__MB_RefreshShopEntry__
#define __ProjectMB__MB_RefreshShopEntry__

#include "MB_FunctionEntranceWnd.h"
class MB_RefreshShopEntry:public MB_FunctionEntranceWnd
{
public:
    virtual bool setDataSource(CCFunctionDataSource* pDataSource);
    
};

#endif /* defined(__ProjectMB__MB_RefreshShopEntry__) */
