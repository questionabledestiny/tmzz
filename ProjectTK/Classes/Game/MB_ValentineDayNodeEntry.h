//
//  MB_ValentineDayNodeEntry.h
//  ProjectMB
//
//  Created by wenyong on 15-1-22.
//
//

#ifndef __ProjectMB__MB_ValentineDayNodeEntry__
#define __ProjectMB__MB_ValentineDayNodeEntry__

#include "MB_NodeFestivalEntery.h"

class MB_ValentineDayNodeEntry : public  MB_NodeFestivalEntery
{
public:
    virtual void onEntranceClicked(CCObject* pSender);
};


#endif /* defined(__ProjectMB__MB_ValentineDayNodeEntry__) */
