//
//  MB_RoleSoundProtocol.h
//  ProjectPM
//
//  Created by WenYong on 14-6-7.
//
//

#ifndef ProjectMB_MB_RoleSoundProtocol_h
#define ProjectMB_MB_RoleSoundProtocol_h

enum EnterSoundValue
{
    kSoundEnterMysteriousShop       = 0,    // 进入补天石商店
    kSoundEnterResearchInstitute    = 1,    // 进入凤王BOSS
    kSoundEnterLLTower              = 2,    // 进入火箭队
    kSoundEnterAbssy                = 3,    // 进入结晶塔
    kSoundEnterTask                 = 4,    // 进入每日任务
    kSoundEnterChampionship         = 5,    // 进入竞技场
    kSoundEnterRace                 = 6,    // 进入华丽大赛
    kSoundEnterHomeLand             = 7,    // 进入炼金工厂
    kSoundEnterToy                  = 8,    // 进入扭蛋
    kSoundEnterKing                 = 9,    // 进入精灵王
    kSoundEnterFormation            = 10,   // 进入队形
    kSoundEnterAdventure            = 11,   // 进入探险
    kSoundEnterFriend               = 12,   // 进入好友
    kSoundEnterLoginReward          = 13,   // 进入登陆奖励
    kSoundEnterVipShop              = 14,   // 进入VIP礼包
    kSoundEnterPay                  = 15,   // 进入进入充值页面
    kSoundEnterCompose              = 16,   // 进入合成页面
    kSoundEnterLevelUp              = 17,   // 进入精炼页面
    kSoundEnterRankUp               = 18,   // 进入进化页面
    kSoundEnterFirstTranform        = 19,   // 进入一转页面
    kSoundEnterSecondTranform       = 20,   // 进入二转页面
    
    kSoundEnterCardLevelUp          = 21,   //宠物升级
    kSoundEnterPve                  = 22,   //进入远征
    kSoundEnterShop_1               = 23,   //进入宝石商店
    kSoundEnterShop_3               = 24,   //进入友好商店
};

#endif
